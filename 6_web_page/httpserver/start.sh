#!/bin/bash

if [[ -n $JARVICE_INGRESSPATH ]]; then
    BASEURL="/$JARVICE_INGRESSPATH/"
else
    BASEURL="/"
fi

PORTNUM=${JARVICE_SERVICE_PORT:-5902}

go run /opt/httpserver/main.go -p "$PORTNUM" -b "$BASEURL"