package main

import (
	"errors"
	"flag"
	"fmt"
	"io"
	"net/http"
	"os"
	"strconv"
)

func getRoot(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "Should not come here\n")
}

func getBaseUrl(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "Hello World ! Jarvice HTTP server\n")
}

func main() {

	// Read JXE arguments
	var port int
	var baseurl string
	var help bool

	flag.IntVar(&port, "p", 80, "Port number of the server")
	flag.StringVar(&baseurl, "b", "/", "Base URL of the server")
	flag.BoolVar(&help, "help", false, "Help")
	flag.Parse()

	// Display help if needed
	if help {
		flag.PrintDefaults()
	} else {

		fmt.Println("Starting server on port ", port, " with baseurl:", baseurl)

		// Starting 2 routes : default and $baseurl/hello
		http.HandleFunc("/", getRoot)
		http.HandleFunc(baseurl+"hello", getBaseUrl)

		// Listen on correct port
		err := http.ListenAndServe(":"+strconv.Itoa(port), nil)
		if errors.Is(err, http.ErrServerClosed) {
			fmt.Printf("server closed\n")
		} else if err != nil {
			fmt.Printf("error starting server: %s\n", err)
			os.Exit(1)
		}
	}
}
