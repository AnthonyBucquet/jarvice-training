#!/bin/bash


if [[ $1 == coll* ]];
then
/opt/JARVICE_UCX/openmpi/bin/mpirun $2 -x LD_LIBRARY_PATH -x PATH --hostfile /etc/JARVICE/nodes --bind-to hwthread --np $JARVICE_MPI_CPU_CORES /opt/osu/libexec/osu-micro-benchmarks/mpi/$1
else
if [[ $(cat /etc/JARVICE/nodes | wc -l) -lt 2 ]]
then
echo "Not enough nodes"
exit 1
fi

head -n 2 /etc/JARVICE/nodes > /tmp/hostfile
cat /tmp/hostfile

/opt/JARVICE_UCX/openmpi/bin/mpirun $2 -x LD_LIBRARY_PATH -x PATH --hostfile /tmp/hostfile --np 2 --bind-to hwthread --map-by ppr:1:node /opt/osu/libexec/osu-micro-benchmarks/mpi/$1
fi